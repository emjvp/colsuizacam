<div class="publicaciones">
    <div class="imagen" >
        <img src="/images/<?php echo $this->publicaciones_inicio->contenidos_imagen?>" alt="">
    </div>
    <div class="container">
        <h3 class="titulo">PUBLICACIONES Y<br>COMUNICACIONES</h3>
        <div class="row">
            <div class="col-lg-5">
                <div class="introduccion">
                    <h3 class="titulo"><?php echo $this->publicaciones->contenidos_titulo;?></h3>
                    <?php echo $this->publicaciones->contenidos_introduccion?>
                    <div class="row">
                        <?php foreach ($this->revistas as $key => $revista) { ?>
                            <div class="col-6">
                                <div class="caja-aliado">
                                    <?php if($revista->revista_pdf){?>
                                        <a href="/files/<?php echo $revista->revista_pdf?>" target="_blank" class="imagen">
                                    <?php }?>
                                        <div>
                                            <img src="/images/<?php echo $revista->revista_imagen;?>" alt="">
                                        </div>
                                    <?php if($revista->revista_pdf){?>
                                        </a>
                                    <?php }?>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-12">
                            <div class="text-right">
                                <a href="/page/revista"><span>OTROS NÚMEROS</span><i class="fas fa-caret-right"></i></a>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>
            <div class="col-lg-7">
                <div class="descripcion">
                    <?php echo $this->publicaciones->contenidos_descripcion?>
                </div>
            </div>
        </div>
    </div>
</div>