<?php
class Page_noticiasController extends Page_mainController
{

	public function indexAction()
	{
	   $contenidosModel = new Page_Model_DbTable_Contenidos();
	   $id = $this->_getSanitizedParam('noticia');
	   $this->_view->noticia = $contenidosModel->getById($id);
	   $fecha = explode("-",$this->_view->noticia->contenidos_fecha);
	   $meses = array('','enero','febrero','marzo','abril','mayo','junio','julio',
		  'agosto','septiembre','octubre','noviembre','diciembre');  
	   $this->_view->fecha = $fecha[2]." ".$meses[$fecha[1]]." ".$fecha[0];
	   
	}
	
}