<div>
<div class="introduccion-aliado" style="background-image:url('/images/<?php if($this->aliado->contenidos_fondo){echo $this->aliado->contenidos_fondo;?>')!important<?php } ?>;">
<div  class="fondo <?php if($this->aliado->contenidos_color == 2){ ?> fondo-rojo<?php } else { ?> fondo-negro <?php } ?>" >
            <ul>
                <li onclick="ShowHide(1)" id="btn-contacto"><a ><span>CONTACTO</span></a></li>
                <li onclick="ShowHide(2)" id="btn-quienes"><a class="activo" ><span>QUÉNES SOMOS</span></a></li>
            </ul>
            <div class="sub-fondo"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-6 col-md-6 col-xl-5">
                    <div class="caja-contenido  <?php if($this->aliado->contenidos_color == 2){?> rojo-descripcion <?php } ?>" >
                            <div id="quienes">
                                <h1 class="titulo <?php if($this->aliado->contenidos_color == 2){?> rojo <?php } ?>"><?php echo $this->aliado->contenidos_titulo?></h1>
                                <div id="pagina1" class="paginacontet">
                                    <div class="introduccion">
                                        <?php echo $this->aliado->contenidos_introduccion;?>
                                    </div>
                                </div>
                                <?php foreach ($this->info as $key => $info) {?>
                                    <div id="pagina<?php echo $key+2; ?>" class="paginacontet" style="display:none;">
                                        <div class="introduccion">
                                            <?php if($info->quienes_titulo){?>
                                                <h2><?php echo $info->quienes_titulo ?></h2>
                                            <?php } ?>    
                                            <?php echo $info->quienes_descripcion; ?> 
                                        </div>
                                    </div>                            
                                <?php } ?>
                                <div class="paginacion-content">
                                    <ul>
                                        <li class="izquierda"><i class="fas fa-caret-left"></i></li>
                                        <li><a data-page="1" class="active">1</a></li>
                                        <?php foreach ($this->info as $key => $info) {?>
                                            <li><a data-page="<?php echo $key+2; ?>"><?php echo $key+2; ?></a></li>
                                        <?php } ?>
                                        <li class="derecha"><i class="fas fa-caret-right"></i></li>
                                    </ul>
                                    <input type="hidden" id="totalpages" value=<?php echo sizeof($this->info)+1;?>>
                                    <input type="hidden" id="pageactual" value="1">
                                </div>

                            </div>
                            <div id="contacto" class="descripcion <?php if($this->aliado->contenidos_color == 2){?> rojo-descripcion <?php } ?>">
                                <h3 class="titulo">CONTACTO</h3>
                                <?php if($this->aliado->contenidos_subtitulo){?>
                                    <h2><?php echo $this->aliado->contenidos_subtitulo?></h2>
                                <?php } ?>
                                <?php echo $this->aliado->contenidos_descripcion?>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>