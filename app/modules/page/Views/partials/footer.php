
<div class="contactanos">
    <div class="container">
		<div class="row">
			<div class="col-12 col-lg-7 col-xl-7">
				<div class="content">
					<h2>CONTÁCTANOS</h2>
					<div class="row">
						<div class="col-12 col-md-6 col-lg-6 col-xl-6">
							<div class="infocontacto1">
								<?php echo $this->infopage->info_pagina_informacion_contacto?>
							</div>
						</div>
						<div class="col-12 col-md-6 col-lg-6 col-xl-6">
							<?php echo $this->infopage->info_pagina_informacion_contacto_footer?>
						</div>
				</div>
				</div>
				</div>
		</div>
		
	</div>
	<div class="mapa">
					<div id="map" style="width:100%; height:100%;display: inline-block;"></div>
				</div>
				<script >

						setValuesMap(<?php echo $this->infopage->info_pagina_latitud;?>, <?php echo $this->infopage->info_pagina_longitud;?>,true,<?php echo $this->infopage->info_pagina_zoom;?>,"");
						google.maps.event.addDomListener(window, 'load', initializeMap);

						</script>
						
				
</div>

