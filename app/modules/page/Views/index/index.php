<?php echo $this->bannerprincipal; ?>
<div class="empresa" align="center">
    <?php echo $this->empresa->contenidos_descripcion?>
</div>
<div class="container">
    <div class="info-suiza-colombia">
        <h2 class="titulo" align="left">SUIZA-COLOMBIA</h2>
        <div class="row">
            <div class="col-12 col-sm-12 col-xl-9">
                <div class="row">
                    <?php foreach ($this->informaciones as $key => $informacion) {?>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="caja">
                                <div class="imagen" style="background-image: url('/images/<?php echo $informacion->contenidos_imagen?>');"></div>
                                <div class="texto">
                                    <h3><?php echo $informacion->contenidos_titulo?></h3>
                                    <span><?php echo strip_tags($informacion->contenidos_introduccion); ?><a href="/page/informacion/?id=<?php echo $informacion->contenidos_id?>"><i class="fas fa-ellipsis-h"></i></a></span>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
                <h2 class="titulo" align="left">NOTICIAS</h2>
                <div class="row">
                    <?php foreach ($this->noticias as $key => $noticia) {?>
                        <div class="col-sm-12 col-md-4 col-lg-4">
                            <div class="caja">
                            <div class="imagen" style="background-image: url('/images/<?php echo $noticia->contenidos_imagen?>');"></div>
                                <div class="texto">
                                    <h3><?php echo $noticia->contenidos_titulo?></h3>
                                    <span><?php echo strip_tags($noticia->contenidos_introduccion); ?><a href="/page/noticias/?noticia=<?php echo $noticia->contenidos_id?>"><i class="fas fa-ellipsis-h"></i></a></span>
                                </div>
                            </div>
                        </div>
                    <?php }?>
                </div>
            </div>
            <div class=" col-md-12 col-xl-3">
            <div class="row">
                <?php foreach ($this->empresas_derecha as $key => $empresa){?>
                    <div class="col-12 col-md-4 col-xl-12">
                        <div class="logos_empresa">
                            <div class="imagen">
                                <img src="/images/<?php echo $empresa->contenidos_imagen?>" alt="">
                            </div>
                        </div>
                    </div>        
                <?php } ?>
            </div>
            </div>
        </div> 
    </div>
    <h2 class="titulo">EVENTOS</h2>
</div>
<div class="eventos">
    <img src="/images/<?php echo $this->eventos_index->contenidos_imagen;?>"  class="imagen-100" alt="">
</div>
<div class="publicaciones">
    <div class="container">
        <h2 class="titulo">PUBLICACIONES</h2>
    </div>
    <div class="imagen">
        <img src="/images/<?php echo $this->publicaciones->contenidos_imagen;?>" alt="">
    </div>
    <div class="informacion">
        <h2><?php echo $this->publicaciones->contenidos_titulo?></h2>
        <?php if($this->publicaciones->enlace){?>
        <a href="<?php echo $this->publicaciones->enlace?>"><span><?php echo $this->publicaciones->contenidos_subtitulo?></span></a><i class="fas fa-angle-right"></i>
        <?php } ?>
    </div>
</div>
<div class="aliados-estrategicos">
    <div class="container">
        <h2 class="titulo">ALIADOS ESTRATÉGICOS</h2>
            <div id='carousel_container'>
                <div class='left_scroll'><i class="fas fa-angle-left"></i></div>
                    <div class="carousel_inner">
                        <ul>
                            <?php foreach ($this->aliados_estrategicos as $key => $aliado) { ?>
                            <li>
                                <div class="imagen">
                                    <img src="/images/<?php echo $aliado->contenidos_imagen?>" alt="">
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                <div class='right_scroll'><i class="fas fa-angle-right"></i></div>
            </div>
    </div>
</div>
