<?php
class Page_acuerdoscomController extends Page_mainController
{

	public function indexAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$this->_view->acuerdos_comerciales = $contenidosModel->getList("contenidos_seccion='14'", "orden ASC")[0];
		$seccionModel = new Page_Model_DbTable_Seccion();
		$contenidosseccionModel = new Page_Model_DbTable_Contenidosseccion();
        $acuerdos = $seccionModel->getList("", "orden ASC");
		$array = array();
		foreach ($acuerdos as $key => $acuerdo) {
			$identificador = $acuerdo->seccion_id;
			$array[$key] = []; 
			$array[$key]['detalle'] = $acuerdo;
			$array[$key]['contenido_seccion'] = $contenidosseccionModel->getList("seccion_id = '$identificador'", "orden ASC");
		}
		$this->_view->acuerdos = $array;
		
	}
}