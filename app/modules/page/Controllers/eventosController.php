<?php
class Page_eventosController extends Page_mainController
{

	public function indexAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$this->_view->eventos_inicio = $contenidosModel->getList("contenidos_seccion = '5'", "orden ASC")[0];
		$this->_view->eventos = $contenidosModel->getList("contenidos_seccion = '13'", "orden ASC");
	}
}