<div class="revistas">
    <div class="container">
        <h1 class="titulo">OTROS NÚMEROS</h1>
        <div class="row">
            <?php foreach ($this->revistas as $key => $revista) {?>
                <div class="col-4">
                    <div class="caja-aliado">
                        <?php if($revista->revista_pdf){?>
                            <a href="/files/<?php echo $revista->revista_pdf?>" target="_blank"><?php } ?>
                            <div class="imagen">
                                <div>
                                    <img src="/images/<?php echo $revista->revista_imagen;?>" alt="">
                                </div>
                            </div>
                        <?php if($revista->revista_pdf){?>
                            </a>
                        <?php } ?>
                        <h2 class="titulo"><?php echo $revista->revista_titulo;?></h2>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
