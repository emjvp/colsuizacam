<div class="contacto">
    <div class="container">
        <h2 class="titulo">INFORMACIÓN GENERAL</h2>
        <?php  if($this->res == 1 ) { ?>
            <div align="center" class="alert alert-success"> El mensaje se envió satisfactoriamente, muy pronto nos pondremos en contacto contigo.</div>
        <?php } else if ($this->res == 2) {?>
            <div class="alert alert-danger">EL mensaje no se pudo enviar, intente de nuevo</div>
        <?php } ?>
        <div class="row">
                <div class="col-md-12 col-lg-6"> 
                    <form method="POST" action="/page/informaciongeneral/enviar4">
                        <div class="row">
                            <div class="col-md-12"> 
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">NOMBRE</label>
                                        <input type="text"  name="nombre" class="form-control" id="inputEmail4" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">EMAIL</label>
                                        <input type="email"  name="email" class="form-control" id="inputEmail4"  required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">TELEFONO</label>
                                        <input type="text"  name="telefono" class="form-control" id="inputEmail4" required>
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label for="inputEmail4">ASUNTO</label>
                                        <input type="text"  name="asunto" class="form-control" id="inputEmail4" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-12">
                                        <label for="inputEmail4">MENSAJE</label>
                                        <textarea type="email" name="mensaje" class="form-control" id="inputEmail4" required></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" id="gridCheck" required>
                                        <label class="form-check-label" for="gridCheck">
                                            Enviar una copia de este correo a mi correo
                                        </label>
                                    </div>
                                </div>
                                <div class="g-recaptcha" data-sitekey="6LekeH0UAAAAAOkaNyklTV7ynbO7ZffMBlcTP3A5"></div>
                                <div align="left" class="boton-contactenos"><button type="submit" class="btn btn-vermas">Enviar</button></div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</div>