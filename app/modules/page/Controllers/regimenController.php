<?php
class Page_regimenController extends Page_mainController
{

	public function indexAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$this->_view->regimen = $contenidosModel->getList("contenidos_seccion = '21'", "orden ASC")[0];
	}
	public function detalleAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$id = $this->_getSanitizedParam('id');
		$this->_view->regimen = $contenidosModel->getById($id);
	}
}