<?php
class Page_impoexpoController extends Page_mainController
{

	public function indexAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$this->_view->impo_expo = $contenidosModel->getList("contenidos_seccion='22'","orden ASC")[0];
		$this->_view->impor = $contenidosModel->getList("contenidos_seccion='23'","orden ASC");
		$this->_view->informacion_impo_expo = $contenidosModel->getList("contenidos_seccion='24'","orden ASC")[0];
		$this->_view->expor = $contenidosModel->getList("contenidos_seccion='25'","orden ASC")[0];
	}
	
}