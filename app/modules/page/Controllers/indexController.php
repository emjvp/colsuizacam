<?php 

/**
*
*/

class Page_indexController extends Page_mainController
{

	public function indexAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$this->_view->bannerprincipal = $this->template->bannerprincipal(1);
		$this->_view->empresa = $contenidosModel->getList("contenidos_seccion = '1'"," orden ASC");
		$this->_view->informaciones = $contenidosModel->getListPages("contenidos_seccion = '2'"," orden ASC ", 0,3);
		$this->_view->empresas_derecha = $contenidosModel->getListPages("contenidos_seccion = '3'"," orden ASC ",0,3);
		$this->_view->noticias = $contenidosModel->getListPages("contenidos_seccion = '4'"," orden ASC ",0,3);
		$this->_view->eventos_index = $contenidosModel->getList("contenidos_seccion = '5'"," orden ASC ")[0];
		$this->_view->publicaciones = $contenidosModel->getList("contenidos_seccion = '6'"," orden ASC ")[0];
		$this->_view->aliados_estrategicos = $contenidosModel->getList("contenidos_seccion = '7'"," orden ASC ");
		
	}
}