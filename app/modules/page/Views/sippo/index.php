<div class="container">
    <div class="sippo">
        <h1 class="titulo"><?php echo $this->sippo_inicio->contenidos_titulo?></h1>
        <div class="row">
            <div class="col-lg-6">
                <?php echo $this->sippo_inicio->contenidos_introduccion;?>
            </div>
            <div class="col-lg-6">
                <div class="imagen" align="center">
                    <img src="/images/<?php echo $this->sippo_inicio->contenidos_imagen;?>" alt="" srcset="">
                </div>
            </div>
        </div>
        <div class="informacion">
            <div class="row linea">
                <?php foreach ($this->sippo as $key => $value) {?>
                    <div class="col-lg-6">
                        <?php echo $value->contenidos_descripcion;?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>