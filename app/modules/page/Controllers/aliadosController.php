<?php
class Page_aliadosController extends Page_mainController
{

	public function indexAction() 
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$filters = "contenidos_seccion = '7'";
		$order = "orden ASC";
		$list = $contenidosModel->getListCount($filters,$order)[0];
		$amount = 12;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		$start = 0;
		$page=1;
		}
		else {
		$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil($list->total/$amount);
		$this->_view->page = $page;
		$this->_view->aliados_estrategicos = $contenidosModel->getListPages($filters,$order,$start,$amount);

    }
	public function detalleAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$quienessomosModel = new Page_Model_DbTable_Quienessomos();
		$id = $this->_getSanitizedParam('id');
		$this->_view->aliado = $contenidosModel->getById($id);
		$this->_view->info = $quienessomosModel->getList("quienes_aliado = '$id'", "orden ASC");
	}
}