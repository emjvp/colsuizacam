<div class="regulaciones-normas">
    <div class="container">
        <h3 class="titulo">REGULACIONES Y NORMAS</h3>
        <div class="row">
            <div class="col-lg-7">
                <div class="introduccion">
                    <h2 class="titulo"><?php echo $this->regulaciones_normas->contenidos_titulo;?></h2>
                    <?php echo $this->regulaciones_normas->contenidos_introduccion;?>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="descripcion">
                    <h2 class="titulo"><?php echo $this->regulaciones_normas->contenidos_subtitulo;?></h2>
                    <?php echo $this->regulaciones_normas->contenidos_descripcion?>
                </div>
            </div>
        </div>
    </div>
</div>