<div class="eventos">
   <img src="/images/<?php echo $this->eventos_inicio->contenidos_imagen;?>"  class="imagen-100" alt="">
    <div>
    <div class="container">
        <div class="seccion-aliados">
        <h2 class="titulo">EVENTOS</h2>
        <div class="aliados">
            <div class="row">
                <?php foreach ($this->eventos as $key => $evento) {?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="caja-aliado">
                            <div class="imagen">
                                <div>
                                <?php if($evento->contenidos_imagen){ ?>
                                    <img src="/images/<?php echo $evento->contenidos_imagen;?>" alt="">
                                <?php } else { ?>
                                    <img src="/skins/page/images/sinimagen.jpg" alt="">
                                <?php } ?>
                                </div>
                            </div>
                            <h2 class="titulo"><?php echo $evento->contenidos_titulo;?></h2>
                            <div class="introduccion-contenidos"><?php echo $evento->contenidos_descripcion;?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        </div>
    </div>
</div>
</div>