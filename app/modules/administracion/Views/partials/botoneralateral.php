<ul>
	<li <?php if($this->botonpanel == 1){ ?>class="activo"<?php } ?>><a href="/administracion/panel"><i class="fas fa-info-circle"></i> Información pagina</a></li>
	<li <?php if($this->botonpanel == 2){ ?>class="activo"<?php } ?>><a href="/administracion/publicidad"><i class="far fa-images"></i> Administrar Banner</a></li>
	<li <?php if($this->botonpanel == 3){ ?>class="activo"<?php } ?>><a href="/administracion/seccion"><i class="far fa-images"></i> Administrar Acuerdos Comerciales</a></li>
	<li <?php if($this->botonpanel == 4){ ?>class="activo"<?php } ?>><a href="/administracion/contenidosseccion"><i class="far fa-images"></i> Administrar Secciones de Acuerdos Comerciales</a></li>
	<li <?php if($this->botonpanel == 5){ ?>class="activo"<?php } ?>><a href="/administracion/contenidos"><i class="fas fa-file-invoice"></i> Administrar Contenidos</a></li>
	<li <?php if($this->botonpanel == 6){ ?>class="activo"<?php } ?>><a href="/administracion/archivostlc"><i class="fas fa-file-invoice"></i> Administrar Archivos TLC</a></li>
	<li <?php if($this->botonpanel == 7){ ?>class="activo"<?php } ?>><a href="/administracion/usuario"><i class="fas fa-users"></i> Administrar Usuarios</a></li>
	<li <?php if($this->botonpanel == 8){ ?>class="activo"<?php } ?>><a href="/administracion/revista"><i class="fas fa-users"></i> Administrar Revistas</a></li>
</ul>