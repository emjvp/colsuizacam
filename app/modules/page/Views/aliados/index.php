<div>
    <div class="container">
        <div class="seccion-aliados">
        <h2 class="titulo">ALIADOS ESTRATÉGICOS</h2>
        <div class="aliados">
            <div class="row">
                <?php foreach ($this->aliados_estrategicos as $key => $aliado) {?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="caja-aliado">
                            <div class="imagen">
                                <div>
                                    <a href="/page/aliados/detalle?id=<?php echo $aliado->contenidos_id?>"><img src="/images/<?php echo $aliado->contenidos_imagen;?>" alt=""></a>
                                </div>
                            </div>
                            <a href="/page/aliados/detalle?id=<?php echo $aliado->contenidos_id?>"><h2 class="titulo"><?php echo $aliado->contenidos_titulo;?></h2></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        </div>
    </div>
</div>
<div class="container">
	<div align="center">
		<ul class="pagination justify-content-center">
			<?php
				$url = '/page/aliados';
				if ($this->totalpages > 1) {
					if ($this->page != 1)
						echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.($this->page-1).'"><i class="fas fa-caret-left"></i></a></li>';
					for ($i=1;$i<=$this->totalpages;$i++) {
						if ($this->page == $i)
							echo '<li class="page-item active"><a class="page-link">'.$this->page.'</a></li>';
						else
							echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.$i.'">'.$i.'</a></li>  ';
					}
					if ($this->page != $this->totalpages)
						echo '<li class="page-item"><a class="page-link" href="'.$url.'?page='.($this->page+1).'"><i class="fas fa-caret-right"></i></a></li>';
				}
			?>
		</ul>
	</div>
</div>