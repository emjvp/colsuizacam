<div>
    <div class="beneficios">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-6 col-lg-5">
                    <h2 class="titulo"><?php echo $this->beneficios->contenidos_titulo;?></h2>    
                    <div class="descripcion"><?php echo $this->beneficios->contenidos_descripcion;?></div>
                </div>
                <div class="col-12 col-md-6 col-lg-7">
                    <div class="text-center">
                        <img src="/images/<?php echo $this->beneficios->contenidos_imagen?>" alt="" srcset="">
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>