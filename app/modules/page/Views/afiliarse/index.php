<div class="afiliarse">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-5">
                <h3 class="titulo"><?php echo $this->afiliacion_informacion->contenidos_titulo?></h3>
                <div class="introduccion">
                    <?php echo $this->afiliacion_informacion->contenidos_introduccion;?>
                </div>
            </div>
            <div class="col-12 col-lg-7">
                <div class="imagen">
                    <img src="/images/<?php echo $this->afiliacion_informacion->contenidos_imagen?>" alt="">
                </div>
                <?php echo $this->afiliacion_informacion->contenidos_descripcion;?>
            </div>
        </div>
    </div>
</div>