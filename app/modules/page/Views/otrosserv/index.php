<div class="otros-servicios">
    <div class="container">
    <h1 class="titulo">OTROS SERVICIOS</h1>
        <div class="seccion">
            <?php foreach ($this->otros_serv as $key => $value) {?>
                <div class="row">
                    <div class="col-12 col-lg-5">
                        <h3 class="titulo"><?php echo $value->contenidos_titulo?></h3>
                        <div class="seccion-int">
                            <?php echo $value->contenidos_introduccion;?>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>