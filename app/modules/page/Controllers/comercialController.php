<?php
class Page_comercialController extends Page_mainController
{

	public function indexAction()
	{
		$this->_view->res = $this->_getSanitizedParam('res');
	} 
	public function enviar2Action()
	{
		$this->setLayout('blanco');
		$data = [''];
		$data ['nombre'] = $this->_getSanitizedParam('nombre'); 
		$data ['email'] = $this->_getSanitizedParam('email');
		$data ['telefono'] = $this->_getSanitizedParam('telefono');
		$data ['asunto'] = $this->_getSanitizedParam('asunto');
		$data ['mensaje'] = $this->_getSanitizedParam('mensaje');
		$email = new Core_Model_Sendingemail($this->_view); 
		$res = $email->enviarcorreo2($data);
		header("Location: /page/comercial?res=".$res);
	}	
}
