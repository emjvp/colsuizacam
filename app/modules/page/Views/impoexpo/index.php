<div class="importacion-exportacion">
    <div class="container">
        <div class="row">
            <div class="col-lg-7">
                <div class="como-importar">
                    <div class="row">
                        <div class="col-lg-6"><h1 class="titulo">CÓMO IMPORTAR</h1></div>
                        <div class="col-lg-6">
                            <div class="imagen">
                                <img src="/images/<?php echo $this->impo_expo->contenidos_imagen?>" alt="" srcset="">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($this->impor as $key => $value) {?>
                            <div class="col-lg-6">
                                <div class="caja">
                                    <div class="icono">
                                        <img src="/images/<?php echo $value->contenidos_imagen?>" alt="">
                                    </div>
                                    <?php echo $value->contenidos_introduccion;?>
                                    <div class="linea" align="center" style="background: #db0819;"></div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>                
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="informacion">
                            <h3 class="titulo"><?php echo $this->informacion_impo_expo->contenidos_titulo;?></h3>
                            <?php echo $this->informacion_impo_expo->contenidos_introduccion?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5">
                <div class="como-exportar">
                    <h2 class="titulo">CÓMO EXPORTAR</h2>
                    <?php echo $this->expor->contenidos_introduccion;?>
                    <div class="informacion">
                        <h3 class="titulo"><?php echo $this->informacion_impo_expo->contenidos_titulo;?></h3>
                        <?php echo $this->informacion_impo_expo->contenidos_introduccion?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>