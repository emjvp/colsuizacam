<div class="header-content">
	<div class="container">
		<div class="row align-items-center">
			<div class="col-6 col-sm-6 col-lg-6 col-xl-3">
				<a href="/"><img src="/skins/page/images/logo.png" class="logo"></a>
			</div>
			<div class="col-6 col-sm-6 d-xl-none">
				<ul align="right" class="adicional-responsive">
					<li id="buscar">
						<span class="lupa-buscar"><i class="fas fa-search"></i></span>
						<form action="" class="form-buscar">
							<div class="row">
								<div class="col-2">
									<span class="lupa-buscar" align="center"><i class="fas fa-search"></i></span>
								</div>
								<div class="col-10">
									<input type="text" class="form-control" name="buscar" id="buscador" placeholder="BUSCAR...">
								</div>
							</div>
						</form>
					</li>						
					<li id="banderas"><img src="/skins/page/images/ukflagcjpg.png" alt="" srcset=""><i class="fas fa-angle-down"></i></li>
					<li id="menu-responsive"><div onclick="menu()"><i class="fas fa-bars"></div></i></li>			
				</ul>
			</div>
			<div class="col-8 d-xl-none">
				<nav class="menu-responsive">
						<ul>
							<div align="center" onclick="menu()"><i class="fas fa-times"></i></div>
							<li><a><span>CÁMARA</span></a>
								<ul class="menu-interno" onclick="menuinterno()">
									<li class="cerrar"><span>Volver</span><i class="fas fa-angle-right"></i></li>
									<li><a href="/page/nosotros"><span>NOSOTROS</span></a>
									<li><a href="/page/aliados"><span>ALIADOS ESTRATÉGICOS</span></a>
								</ul>
							</li>
							<li><a><span>AFILIADOS</span></a>
								<ul class="menu-interno" onclick="menuinterno()">
									<li class="cerrar"><span>Volver</span><i class="fas fa-angle-right"></i></li>
									<li><a href="/page/miembros"><span>MIEMBROS</span></a>
									<li><a href="/page/beneficios"><span>BENEFICIOS</span></a>
									<li><a href="/page/afiliarse"><span>COMO AFILIARSE</span></a>
								</ul>
							</li>
							<li><a><span>SERVICIOS</span></a>
								<ul class="menu-interno" onclick="menuinterno()">
									<li class="cerrar"><span>Volver</span><i class="fas fa-angle-right"></i></li>
									<li><a href="/page/promocion"><span>PROMOCIÓN Y C.COMERCIALES</span></a>
									<li><a href="/page/eventos"><span>EVENTOS</span></a>
									<li><a href="/page/publicaciones"><span>PUBLICACIONES Y COMUNICACIONES</span></a>
									<li><a href="/page/otrosserv"><span>OTROS SERVICIOS</span></a>
								</ul>
							</li>
							<li><a><span>RÉGIMEN</span></a>
								
									<ul class="menu-interno" onclick="menuinterno()">
										<li class="cerrar"><span>Volver</span><i class="fas fa-angle-right"></i></li>
										<?php foreach ($this->regimen as $key => $regimen) { ?>
											<li><a href="/page/regimen/detalle?id=<?php echo $regimen->contenidos_id ?>"><span><?php echo $regimen->contenidos_titulo?></span></a>
										<?php } ?>
									</ul>
									
							</li>
							<li><a><span>COMERCIO</span></a>
								<ul class="menu-interno" onclick="menuinterno()">
									<li class="cerrar"><span>Volver</span><i class="fas fa-angle-right"></i></li>
									<li><a href="/page/acuerdoscom"><span>ACUERDOS COMERCIALES</span></a></li>
									<li><a href="/page/regulaciones"><span>REGULACIONES Y NORMAS</span></a></li>
									<li><a href="/page/impoexpo"><span>CÓMO IMPORTAR CÓMO EXPORTAR</span></a></li>
									<li><a href="/page/tlccolombia"><span>TLC COLOMBIA EFTA</span></a></li>
									<li><a href="/page/inversionsuiza"><span>INVERSIÓN SUIZA EN COLOMBIA</span></a></li>
									<li><a href="/page/sippo"><span>SIPPO</span></a></li>
								</ul>
							</li>
							<li><a><span>CONTACTO</span></a>
								<ul class="menu-interno" onclick="menuinterno()">
									<li class="cerrar"><span>Volver</span><i class="fas fa-angle-right"></i></li>
									<li><a href="/page/inscripcion"><span>INSCRIPCIÓN</span></a></li>
									<li><a href="/page/comercial"><span>COMERCIAL</span></a></li>
									<li><a href="/page/proyectos"><span>PROYECTOS</span></a></li>
									<li><a href="/page/informaciongeneral"><span>INFORMACIÓN GENERAL</span></a></li>
								</ul>
							</li>
						</ul>
				</nav>
			</div>
			
			<div class="col-sm-9 col-lg-9 col-xl-9 d-xl-block d-none">
				<nav class="botonera">
					<ul>
						<li><a><span>CÁMARA</span></a>
							<ul>
								<li><a href="/page/nosotros"><span>NOSOTROS</span></a>
								<li><a href="/page/aliados"><span>ALIADOS ESTRATÉGICOS</span></a>
							</ul>
						</li>
						<li><a><span>AFILIADOS</span></a>
							<ul>
								<li><a href="/page/miembros"><span>MIEMBROS</span></a>
								<li><a href="/page/beneficios"><span>BENEFICIOS</span></a>
								<li><a href="/page/afiliarse"><span>COMO AFILIARSE</span></a>
							</ul>
						</li>
						<li><a><span>SERVICIOS</span></a>
							<ul>
								<li><a href="/page/promocion"><span>PROMOCIÓN Y C.COMERCIALES</span></a>
								<li><a href="/page/eventos"><span>EVENTOS</span></a>
								<li><a href="/page/publicaciones"><span>PUBLICACIONES Y COMUNICACIONES</span></a>
								<li><a href="/page/otrosserv"><span>OTROS SERVICIOS</span></a>
							</ul>
						</li>
						<li><a><span>RÉGIMEN</span></a>
							
								<ul class="varios lista">
									<?php foreach ($this->regimen as $key => $regimen) { ?>
										<li><a href="/page/regimen/detalle?id=<?php echo $regimen->contenidos_id ?>"><span><?php echo $regimen->contenidos_titulo?></span></a>
									<?php } ?>
								</ul>
								
						</li>
						<li><a><span>COMERCIO</span></a>
							<ul>
								<li><a href="/page/acuerdoscom"><span>ACUERDOS COMERCIALES</span></a></li>
								<li><a href="/page/regulaciones"><span>REGULACIONES Y NORMAS</span></a></li>
								<li><a href="/page/impoexpo"><span>CÓMO IMPORTAR CÓMO EXPORTAR</span></a></li>
								<li><a href="/page/tlccolombia"><span>TLC COLOMBIA EFTA</span></a></li>
								<li><a href="/page/inversionsuiza"><span>INVERSIÓN SUIZA EN COLOMBIA</span></a></li>
								<li><a href="/page/sippo"><span>SIPPO</span></a></li>
							</ul>
						</li>
						<li><a><span>CONTACTO</span></a>
							<ul>
								<li><a href="/page/inscripcion"><span>INSCRIPCIÓN</span></a></li>
								<li><a href="/page/comercial"><span>COMERCIAL</span></a></li>
								<li><a href="/page/proyectos"><span>PROYECTOS</span></a></li>
								<li><a href="/page/informaciongeneral"><span>INFORMACIÓN GENERAL</span></a></li>
							</ul>
						</li>
						<li id="buscar">
							<span class="lupa-buscar"><i class="fas fa-search"></i></span>
							<form action="" class="form-buscar">
								<div class="row">
									<div class="col-2">
										<span class="lupa-buscar" align="center"><i class="fas fa-search"></i></span>
									</div>
									<div class="col-10">
										<input type="text" class="form-control" name="buscar" id="buscador" placeholder="BUSCAR...">
									</div>
								</div>
							</form>

						</li>
						<li id="banderas">
							<a><img src="/skins/page/images/colombia.png" alt="" srcset=""><i class="fas fa-angle-down"></i></a>
							<ul>
								<li><a><img src="/skins/page/images/ukflag.jpg" alt="" srcset=""></i></a></li>
							</ul>
						</li>

					</ul>
					</div>
				</nav>
			</div>
		</div>
	</div>
</div>