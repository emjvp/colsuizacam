<div class="tlc">
    <div class="container">
        <h2 class="titulo">TLC COLOMBIA EFTA</h2>
        <?php foreach ($this->tlc as $key => $tlc) { ?>
            <div><?php echo $tlc->contenidos_introduccion ?></div>
        <?php } ?>
    </div>
    <div class="container">
        <div class="row">
            <?php foreach ($this->archivostlc as $key => $archivos) { ?>
                <div class="col-sm-4">
                    <div class="boton-tlc">
                        <a href="/files/<?php echo $archivos->archivostlc_archivo ?>" class="btn btn-vermas"><?php echo $archivos->archivostlc_titulo ?></a>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>