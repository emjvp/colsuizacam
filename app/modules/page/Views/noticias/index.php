<div class="noticias-seccion">
    <div class="otros-servicios">
        <div class="container">
            <div class="seccion">
                <div class="row">
                    <div class="col-12 col-xl-10">
                        <h3 class="titulo"><?php echo $this->noticia->contenidos_titulo;?></h3>
                        <span class="fecha"><?php echo $this->fecha?></span>
                        <div class="seccion-int">
                            <div class="imagen" style="background-image: url('/images/<?php echo $this->noticia->contenidos_imagen?>');">
                            </div>
                            <div class="descripcion">
                                <?php echo $this->noticia->contenidos_descripcion;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>