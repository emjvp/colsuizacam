<div class="miembros">
    <div class="introduccion-aliado" style="background-image:url('/images/<?php if($this->miembro->contenidos_fondo){echo $this->miembro->contenidos_fondo;?>')<?php } ?>;">
        <div class="fondo">
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-lg-5">
                    <h2 class="titulo <?php if($this->miembro->contenidos_color == 2){?> rojo <?php } ?>"><?php echo $this->miembro->contenidos_titulo?></h2>
                    <div class="descripcion <?php if($this->miembro->contenidos_color == 2){?> rojo-descripcion <?php } ?>">
                        <?php echo $this->miembro->contenidos_descripcion?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>