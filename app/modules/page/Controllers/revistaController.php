<?php
class Page_revistaController extends Page_mainController
{

	public function indexAction()
	{
		$revistaModel = new Page_Model_DbTable_Revista();
		$this->_view->revistas = $revistaModel->getList("", "orden ASC");
	}
	
}