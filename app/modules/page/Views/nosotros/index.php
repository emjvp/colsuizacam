<div>
    <div class="introduccion" style="background-image: url('/images/<?php echo $this->introduccion->contenidos_imagen ?>')">
    <div class="fondo">
    </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-6 col-lg-5">
                    <h2 class="titulo"><?php echo $this->introduccion->contenidos_titulo;?></h2>    
                    <div class="descripcion"><?php echo $this->introduccion->contenidos_descripcion;?></div>
                </div>
            </div>
        </div>
    </div>
    <div class="mision-vision">
        <div class="container">
            <div class="row align-items-center">
                <?php foreach ($this->mision_vision as $key => $value) {?>
                    <div class="col-md-8 col-sm-12 col-lg-4 linea align-self-start">
                        <div class="caja">
                            <h3 class="titulo"><?php echo $value->contenidos_titulo?></h3>
                            <div class="descripcion">
                                <?php echo $value->contenidos_introduccion?>
                            </div>
                        </div>
                    </div>
                <?php }?>
                <div class=" col-12 col-sm-12 col-md-4 col-lg-4 align-self-start">
                    <div class="imagen">
                        <img src="/skins/page/images/icono.png" alt="" srcset="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>