<?php
class Page_miembrosController extends Page_mainController
{

	public function indexAction() 
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$filters = "contenidos_seccion = '20'";
		$order = "orden ASC";
		$list = $contenidosModel->getListCount($filters,$order)[0];
		$amount = 12;
		$page = $this->_getSanitizedParam("page");
		if (!$page) {
		$start = 0;
		$page=1;
		}
		else {
		$start = ($page - 1) * $amount;
		}
		$this->_view->totalpages = ceil($list->total/$amount);
		$this->_view->page = $page;
		$this->_view->miembros = $contenidosModel->getListPages($filters,$order,$start,$amount);
    }
	public function detalleAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$id = $this->_getSanitizedParam('id');
		$this->_view->miembro = $contenidosModel->getById($id);
	}
}