<?php
class Page_publicacionesController extends Page_mainController
{

	public function indexAction()
	{
		$contenidosModel = new Page_Model_DbTable_Contenidos();
		$revistaModel = new Page_Model_DbTable_Revista();
		$this->_view->publicaciones_inicio = $contenidosModel->getList("contenidos_seccion = '6'", "orden ASC")[0];
		$this->_view->publicaciones = $contenidosModel->getList("contenidos_seccion = '18'", "orden ASC")[0];
		$this->_view->revistas = $revistaModel->getListPages("","orden ASC",0,2);
		
	}
}