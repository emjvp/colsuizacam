<?php
class Page_tlccolombiaController extends Page_mainController
{

	public function indexAction()
	{
        $contenidosModel = new Page_Model_DbTable_Contenidos();
		$this->_view->tlc = $contenidosModel->getList("contenidos_seccion = '28'", "orden ASC");
		$archivostlcModel = new Page_Model_DbTable_Archivostlc();
		$this->_view->archivostlc = $archivostlcModel->getList("", "orden ASC");
	}
	
}