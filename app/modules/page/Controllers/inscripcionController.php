<?php
class Page_inscripcionController extends Page_mainController
{

	public function indexAction()
	{
		$this->_view->res = $this->_getSanitizedParam('res');
	} 
	public function enviarAction()
	{
		$this->setLayout('blanco');
		$data = [''];
		$data ['nombre'] = $this->_getSanitizedParam('nombre'); 
		$data ['empresa'] = $this->_getSanitizedParam('empresa');
		$data ['id'] = $this->_getSanitizedParam('id');
		$data ['persona'] = $this->_getSanitizedParam('persona');
		$data ['email'] = $this->_getSanitizedParam('email');
		$data ['index'] = $this->_getSanitizedParam('index');
		$data ['telefono'] = $this->_getSanitizedParam('persona');
		$data ['codigo'] = $this->_getSanitizedParam('persona');
		$email = new Core_Model_Sendingemail($this->_view); 
		$res = $email->enviarcorreo($data);
		header("Location: /page/inscripcion?res=".$res);
	}	
}