<div>
    <div class="container">
        <div class="seccion-aliados">
        <h2 class="titulo">PROMOCIÓN Y<br>CONTACTOS COMERCIALES</h2>
        <div class="aliados">
            <div class="row">
                <?php foreach ($this->informacion as $key => $value) {?>
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="caja-aliado">
                            <div class="imagen">
                                <div>
                                <?php if($value->contenidos_imagen){ ?>
                                    <img src="/images/<?php echo $value->contenidos_imagen;?>" alt="">
                                <?php } else { ?>
                                    <img src="/skins/page/images/sinimagen.jpg" alt="">
                                <?php } ?>
                                </div>
                            </div>
                            <h2 class="titulo"><?php echo $value->contenidos_titulo;?></h2>
                            <div class="introduccion-contenidos"><?php echo $value->contenidos_descripcion;?></div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        </div>
    </div>
</div>